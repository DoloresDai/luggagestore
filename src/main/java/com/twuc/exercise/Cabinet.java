package com.twuc.exercise;

import com.twuc.exercise.exceptions.InvalidTicket;

import java.util.HashMap;
import java.util.Map;

public class Cabinet {
    private Map<Ticket,Luggage> locker = new HashMap<>();


    public Ticket deposit(Luggage luggage) {
        Ticket ticket = new Ticket();
        this.locker.put(ticket, luggage);
        return ticket;
    }

    public Luggage claim(Ticket ticket) {
        if (!locker.containsKey(ticket)){
            throw new InvalidTicket();
        }
        return locker.remove(ticket);
    }
}
