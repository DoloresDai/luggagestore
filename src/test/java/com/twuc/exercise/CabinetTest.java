package com.twuc.exercise;

import com.twuc.exercise.exceptions.InvalidTicket;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CabinetTest {

    private Cabinet cabinet;
    private Luggage luggage;

    @BeforeEach
    void setUp() {
        cabinet = new Cabinet();
        luggage = new Luggage();
    }

    @Test
    void should_return_ticket_when_deposit_a_luggage() {
        Cabinet cabinet = new Cabinet();
        Ticket ticket = cabinet.deposit(new Luggage());
        assertNotNull(ticket);
    }

    @Test
    void should_return_luggage_when_submit_ticket() {
        Luggage myLuggage = new Luggage();
        Ticket ticket = cabinet.deposit(myLuggage);

        Luggage claim = cabinet.claim(ticket);

        assertEquals(myLuggage, claim);
    }

    @Test
    void should_return_my_first_luggage_when_claim() {
        Luggage myLuggage = new Luggage();
        Ticket ticket = cabinet.deposit(myLuggage);
        Ticket ticket2 = cabinet.deposit(new Luggage());

        Luggage claim = cabinet.claim(ticket);

        assertEquals(myLuggage, claim);
    }

    @Test
    void should_return_null_luggage_when_deposit_null() {
        Ticket ticket = cabinet.deposit(null);

        Luggage nullLuggage = cabinet.claim(ticket);

        assertNull(nullLuggage);
    }

    @Test
    void should_throw_when_claim_useless_ticket() {
        Ticket ticket = cabinet.deposit(luggage);
        Luggage luggage = cabinet.claim(ticket);

        assertThrows(InvalidTicket.class, () -> cabinet.claim(ticket));
    }

    @Test
    void should_throw_when_claim_invalid_ticket() {
        assertThrows(InvalidTicket.class, () -> cabinet.claim(new Ticket()));
    }
}
